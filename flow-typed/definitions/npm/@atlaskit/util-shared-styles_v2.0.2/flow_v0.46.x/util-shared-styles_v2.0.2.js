declare module '@atlaskit/util-shared-styles' {
  declare var akBorderRadius: string;

  // Primary color palette
  declare var akColorPrimary1: string;
  declare var akColorPrimary2: string;
  declare var akColorPrimary3: string;

  // Secondary color palette
  declare var akColorSecondary1: string;
  declare var akColorSecondary2: string;
  declare var akColorSecondary3: string;
  declare var akColorSecondary4: string;
  declare var akColorSecondary5: string;

  // Reds
  declare var akColorR50: string;
  declare var akColorR75: string;
  declare var akColorR100: string;
  declare var akColorR200: string;
  declare var akColorR300: string;
  declare var akColorR400: string;
  declare var akColorR500: string;

  // Yellows
  declare var akColorY50: string;
  declare var akColorY75: string;
  declare var akColorY100: string;
  declare var akColorY200: string;
  declare var akColorY300: string;
  declare var akColorY400: string;
  declare var akColorY500: string;

  // Greens
  declare var akColorG50: string;
  declare var akColorG75: string;
  declare var akColorG100: string;
  declare var akColorG200: string;
  declare var akColorG300: string;
  declare var akColorG400: string;
  declare var akColorG500: string;

  // Blues
  declare var akColorB50: string;
  declare var akColorB75: string;
  declare var akColorB100: string;
  declare var akColorB200: string;
  declare var akColorB300: string;
  declare var akColorB400: string;
  declare var akColorB500: string;

  // Purples
  declare var akColorP50: string;
  declare var akColorP75: string;
  declare var akColorP100: string;
  declare var akColorP200: string;
  declare var akColorP300: string;
  declare var akColorP400: string;
  declare var akColorP500: string;

  // Teals
  declare var akColorT50: string;
  declare var akColorT75: string;
  declare var akColorT100: string;
  declare var akColorT200: string;
  declare var akColorT300: string;
  declare var akColorT400: string;
  declare var akColorT500: string;
  declare var akColorN0: string;
  declare var akColorN10: string;
  declare var akColorN20: string;
  declare var akColorN30: string;
  declare var akColorN40: string;
  declare var akColorN50: string;
  declare var akColorN60: string;
  declare var akColorN70: string;
  declare var akColorN80: string;
  declare var akColorN90: string;
  declare var akColorN100: string;
  declare var akColorN200: string;
  declare var akColorN300: string;
  declare var akColorN400: string;
  declare var akColorN500: string;
  declare var akColorN600: string;
  declare var akColorN700: string;
  declare var akColorN800: string;
  declare var akColorN900: string;
  declare var akColorN10A: string;
  declare var akColorN20A: string;
  declare var akColorN30A: string;
  declare var akColorN40A: string;
  declare var akColorN50A: string;
  declare var akColorN60A: string;
  declare var akColorN70A: string;
  declare var akColorN80A: string;
  declare var akColorN90A: string;
  declare var akColorN100A: string;
  declare var akColorN200A: string;
  declare var akColorN300A: string;
  declare var akColorN400A: string;
  declare var akColorN500A: string;
  declare var akColorN600A: string;
  declare var akColorN700A: string;
  declare var akColorN800A: string;
  declare var akFontFamily: string;
  declare var akFontSizeDefault: string;
  declare var akCodeFontFamily: string;
  declare var akGridSizeUnitless: number;
  declare var akGridSize: string;

  // Z-INDEX
  declare var akZIndexNavigation: number;
  declare var akZIndexLayer: number;
  declare var akZIndexBlanket: number;
  declare var akZIndexFlag: number;
  declare var akZIndexCard: number;
  declare var akZIndexDialog: number;
  declare var akZIndexModal: number;

  declare var akTypographyMixins: {
    h100: string,
    h200: string,
    h300: string,
    h400: string,
    h500: string,
    h600: string,
    h700: string,
    h800: string,
    h900: string,
  };
}
