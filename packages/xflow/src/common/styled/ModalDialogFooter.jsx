import styled from 'styled-components';

const ModalDialogFooter = styled.div`
  text-align: right;
  padding: 14px 20px 20px;
`;

ModalDialogFooter.displayName = 'InitializingScreenFooter';
export default ModalDialogFooter;
