import styled from 'styled-components';

const OptOutMessage = styled.div`
  padding: 0px 20px 20px 0px;
`;

OptOutMessage.displayName = 'OptOutMessage';
export default OptOutMessage;
