import styled from 'styled-components';

const OptOutFooter = styled.div`
  text-align: right;
  padding: 14px 20px 20px;
`;

OptOutFooter.displayName = 'OptOutFooter';
export default OptOutFooter;
